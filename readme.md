# GitLab-CI template for DefectDojo: Secret Detection

This repository provides an example implementation of [DefectDojo](https://www.defectdojo.org) with [GitLab-CI](https://docs.gitlab.com/ee/ci/).

It supports [GitLab Secret Detection](https://docs.gitlab.com/ee/user/application_security/secret_detection/) tests.

As soon as you include this implementation the GitLab Secret Detection test will be activated.

It will scan the hardcoded secrets in the project using the GitLab Secret Detection tool and send the scan results to DefectDojo.
It will create Jobs at the pre, post and test stages in the CI/CD pipeline:
The pre-stage Job will be to create an engagement in DefectDojo, which will include the project URL, CI Pipeline and Build ID, CI commit hash, and other relevant information.
During the testing stage Job, the project will be scanned for the hard-coded secret and the scan result will be saved as an artifact.
As a post-stage Job, it will upload the GitLab Secret Detection scan report to the created interaction in DefectDojo.


## Usage
For using this example simple include the main file "gitlab-defectdojo.yml" as a remote and configure at least the mandatory variables in your CI/CD pipeline.

```
include:
  - remote: "https://gitlab.com/aktailak01/gitlabci-defectdojo/-/raw/master/gitlab-defectdojo.yml"
````

## Variables

The variables have to be set in your gitlab-ci.yml file or in the GitLab UI.

### General

| Variable        | Mandatory | Default | Description |
| -------------   |:-------------:| -----:| -----: |
| DEFECTDOJO_URL | Yes | null | URL your your DefektDojo API-V2 Endpoint (https://defectdojo.example.com/api/v2) |
| DEFECTDOJO_TOKEN | Yes | null | API token for API-V2 Endpoint| 
| DEFECTDOJO_PRODUCTID | Yes | null | ID of your Product in DefectDojo |
| DEFECTDOJO_NOT_ON_MASTER | No | false | Disable DefectDojo implementation when executed on Master branch |

### Engagement
| Variable        | Mandatory | Default | Description |
| -------------   |:-------------:| -----:| -----: |
| DEFECTDOJO_ENGAGEMENT_PERIOD | No | 7 | Duration in days of the created Engagement |
| DEFECTDOJO_ENGAGEMENT_STATUS | No | Not Started | Initial Status of the Engagement when created. Possible Values: Not Started, Blocked, Cancelled, Completed, In Progress, On Hold, Waiting for Resource |
| DEFECTDOJO_ENGAGEMENT_DEDUPLICATION_ON_ENGAGEMENT | No | false | If enabled deduplication will only mark a finding in this engagement as duplicate of another finding if both findings are in this engagement. If disabled, deduplication is on the product level. | 
| DEFECTDOJO_ENGAGEMENT_BUILD_SERVER | No | null | ID of the Build Server if configured in DefecDojo | 
| DEFECTDOJO_ENGAGEMENT_SOURCE_CODE_MANAGEMENT_SERVER | No | null | ID of the SCM Server if configured in DefecDojo |
| DEFECTDOJO_ENGAGEMENT_ORCHESTRATION_ENGINE | No | null | ID of the Orchestration Engine if configured in DefecDojo | 
| DEFECTDOJO_ENGAGEMENT_THREAT_MODEL | No | true | |
| DEFECTDOJO_ENGAGEMENT_API_TEST | No | true | |
| DEFECTDOJO_ENGAGEMENT_PEN_TEST | No | true | |
| DEFECTDOJO_ENGAGEMENT_CHECK_LIST | No | true | |

### Scan
| Variable        | Mandatory | Default | Description |
| -------------   |:-------------:| -----:| -----: |
| DEFECTDOJO_SCAN_MINIMUM_SEVERITY | No | Info | Available values : Info, Low, Medium, High, Critical | 
| DEFECTDOJO_SCAN_ACTIVE | No | true | |
| DEFECTDOJO_SCAN_VERIFIED | No | true | |
| DEFECTDOJO_SCAN_CLOSE_OLD_FINDINGS | No | true | |
| DEFECTDOJO_SCAN_PUSH_TO_JIRA | No | false | |
| DEFECTDOJO_SCAN_ENVIRONMENT | No | Default | |

### Secret Detection
| Variable        | Mandatory | Default | Description |
| -------------   |:-------------:| -----:| -----: |
| SECRET_DETECTION_HISTORIC_SCAN | No | false | | 

## Forking

Feel free to fork this repository and include into your own GitLab Instance. Credit to Stefan Steinert.
